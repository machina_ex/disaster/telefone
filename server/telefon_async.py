import asyncio
import json
import serial_asyncio
import serial.tools.list_ports
import random
import enum
import aiohttp
import aiohttp.web
# {"start":"clip name"}
# {"start":{[szene|clip]:"name"}}
# {"stop:{[track|clip]:"name"}}
# {"mix":{"track":"TrackName","send":"a-Z0-09","value":0-1.0}}

class Telefon:
	def __init__(self,raetsel,port,livePrefix):
		self.raetsel = raetsel
		self.port = port
		self.keyListeners = []
		self.livePrefix = livePrefix

	def __await__(self):
		self.reader, self.writer = yield open_serial_connection(url='hwgrep://'+self.port, baudrate=115200) # (url='hwgrep://'+self.port, baudrate=115200)
		asyncio.ensure_future(self.readerTask())
		
		self.disconnectAllMics()

		return self

	async def readerTask(self):
		while True:
			msg = await self.reader.readuntil(b'\n')
			_, key, event = msg.split(b" ")
			#print (f"{key} {event}")

			if b"RELEASED." in event:
				ev = 'kR'

			if b"PRESSED." in event:
				ev = 'kP'

			for listener in filter(lambda x: x[0] == str(key,"utf8") or x[0]=='',self.keyListeners ):
				listener[1].set_result((key,ev))
				self.keyListeners.remove(listener)

	async def keyEvent(self, key):
		loop = asyncio.get_event_loop()
		fut = loop.create_future()
		self.keyListeners.append((key,fut))
		key, event =await fut
		return key, event, self

	def connectMicTo(self, tel):
		self.raetsel.mixerSet(f"{self.livePrefix} Telefon MIC", int(tel.livePrefix)-1, 1)

	def disconnectMicFrom(self, tel):
		self.raetsel.mixerSet(f"{self.livePrefix} Telefon MIC", int(tel.livePrefix)-1, 0)

	def setOutToLoudspeaker(self):
		self.raetsel.mixerSet(f"{self.livePrefix} Telefon OUT", "panning", -1)

	def setOutToReceiver(self):
		self.raetsel.mixerSet(f"{self.livePrefix} Telefon OUT", "panning", 1)

	def startClip(self,clip):
		self.raetsel.startClip(f"{self.livePrefix} {clip}")

	def disconnectAllMics(self):
		for i in range(1,12):
			self.raetsel.mixerSet(f"{self.livePrefix} Telefon MIC", i-1, 0)

	def stopClips(self):
		self.raetsel.stopTrack(f"{self.livePrefix} Telefon OUT")


class states(enum.Enum):
	STOP = enum.auto()
	START = enum.auto()
	WARTEN = enum.auto()
	CONNECTED = enum.auto()
	DIALOG = enum.auto()
	WIN_DIALOG = enum.auto()
	WIN_CODE = enum.auto()


class Person:
	def __init__(self, raetsel, telefon):
		self.raetsel = raetsel
		self.tel = telefon
		self.state = states.STOP
		self.partner = None
		self.keysPressed = b""

		self.dialog = None
		self.dialogRole = ""
		asyncio.ensure_future(self.keysTask())

	async def keysTask(self):
		while True:
			key,event,_ = await self.tel.keyEvent('')
			self.raetselStateMachine(event,key)


	def raetselStateMachine(self,event,msg = ""):
		fromstate = self.state
		if event == "kP":
			self.keysPressed += msg
			print(f"{self.tel.port}: {self.keysPressed}")

		#state transitions
		if self.state == states.START and event == "kR" and msg == b"H":
			self.state = states.WARTEN

		if self.state == states.WARTEN and event == "foundPartner":
			self.state = states.CONNECTED

		if self.state  == states.CONNECTED and event == "lostPartner":
			self.state = states.WARTEN

		if self.state == states.CONNECTED and self.keysPressed.endswith(bytearray(self.partner.tel.port,"utf8")):
			self.state = states.WIN_CODE

		if self.state == states.WIN_CODE and event == "lostPartner":
			self.state = states.WARTEN

		if self.state == states.WIN_CODE and self.partner.state == states.WIN_CODE:
			self.state = states.DIALOG
			self.partner.state = states.DIALOG
			self.partner.runStateMachineActions()

		if self.state in [states.WARTEN, states.CONNECTED, states.WIN_CODE] and event == "kP" and msg == b"H":
			self.state = states.START

		if self.state == states.DIALOG and self.keysPressed.endswith(self.dialog[1]):
			self.state = states.WIN_DIALOG
			self.partner.state = states.WIN_DIALOG
			self.partner.runStateMachineActions()

		if fromstate != self.state:
			print(f"{self.tel.port}: {fromstate.name}->{self.state.name}")
			self.runStateMachineActions()
			self.raetsel.sendState(self)

	def runStateMachineActions(self):
		#actions
		if self.state == states.STOP:
			self.lostPartner()
			self.tel.disconnectAllMics()
			self.tel.stopClips()
		elif self.state == states.START:
			self.lostPartner()
			self.tel.setOutToLoudspeaker()
			self.tel.startClip("Ring")
		elif self.state == states.WARTEN:
			self.lostPartner()
			self.tel.setOutToReceiver()
			self.tel.startClip("Warten")
			self.raetsel.queueForPartner(self)
		elif self.state == states.CONNECTED:
			self.tel.startClip("Code Eingeben")
		elif self.state == states.WIN_CODE:
			self.tel.startClip("Warten")
		elif self.state == states.DIALOG:
			self.tel.startClip(f"DIALOG_{self.dialog[0]}_{self.dialogRole}")
		elif self.state == states.WIN_DIALOG:
			self.tel.startClip("Info")
			self.tel.disconnectMicFrom(self.partner.tel)

	def startRandomly(self):
		loop = asyncio.get_event_loop()
		self.state = states.START
		loop.call_later(random.random()*10,self.runStateMachineActions)

	def setState(self,state):
		if state in [states.CONNECTED, states.DIALOG, states.WIN_DIALOG, states.WIN_CODE]:
			if not self.partner:
				print(f"{self.tel.port} unable to set state to {state} as there is no partner")
				return
			else:
				self.partner.state = state
				self.partner.runStateMachineActions()

		self.state = state
		runStateMachineActions()

	def foundPartner(self, pers):
		self.partner = pers
		self.raetselStateMachine("foundPartner")
		#let the other person hear my voice
		self.tel.connectMicTo(self.partner.tel)


	def isWaitingForPartner(self):
		return self.state == states.WARTEN

	def lostPartner(self):
		if self.partner:
			self.raetsel.freeDialog(self.dialog)
			self.dialog = None
			p = self.partner
			self.partner = None
			self.tel.disconnectMicFrom(p.tel)
			p.lostPartner()
			self.raetselStateMachine("lostPartner")

	def setDialog(self, dialog, role):
		self.dialog = dialog
		self.dialogRole = role




class Telefonraetsel:
	def __init__(self,serialNrToLiveTrack, dialogs, serverUrl):
		self.personen = []
		self.dialogs = dialogs
		self.serialNrToLiveTrack = serialNrToLiveTrack
		self.abletonLiveWriter = None
		self.waitingForPartner = None
		self.serverUrl = serverUrl
	def __await__(self):
		loop = asyncio.get_event_loop()

		self.session = aiohttp.ClientSession()
		server = aiohttp.web.Server(self.controlRequestHandler)
		asyncio.ensure_future(loop.create_server(server, "0.0.0.0", 8082)) # 8080

		_, self.abletonLiveWriter = yield from asyncio.open_connection('127.0.0.1', 9000).__await__() # 127.0.0.1

		for serialNr, livePrefix in self.serialNrToLiveTrack.items():
			tel = yield from Telefon(self, serialNr, livePrefix).__await__()
			pers = Person(self, tel)
			self.personen.append(pers)
		self.startClip("noise")
		return self

	async def controlRequestHandler(self,request):
		json = await request.json()
		print(json)
		if "start" in json and json["start"] == "allRandom":
			for pers in self.personen:
				pers.startRandomly()
		elif "stop" in json and json["stop"] == "all" :
			for pers in self.personen:
				pers.state = states.STOP
				pers.runStateMachineActions()
		else:
			for pers in self.personen:
				if pers.tel.port in json and json[pers.tel.port] in states:
					pers.setState(states[json[pers.tel.port]])
		return aiohttp.web.Response(text="OK")

	def sendState(self,p):
		if p.partner:
			d = {p.tel.port:{"state":p.state.name, "keysPressed":str(p.keysPressed,"utf8"), "partner":p.partner.tel.port}}
		else:
			d = {p.tel.port:{"state":p.state.name, "keysPressed":str(p.keysPressed,"utf8")}}
		asyncio.ensure_future(self.session.get(self.serverUrl, json=d))

	def startClip(self,clip):
		s = {"start":{"clip":clip}}
		self.abletonLiveWriter.write(bytearray(json.dumps(s)+"\n","utf8"))
		asyncio.create_task(self.abletonLiveWriter.drain())

	def startTrack(self,track):
		s = {"start":{"track":track}}
		self.abletonLiveWriter.write(bytearray(json.dumps(s)+"\n","utf8"))
		asyncio.create_task(self.abletonLiveWriter.drain())

	def stopClip(self,clip):
		s = {"stop":{"clip":clip}}
		self.abletonLiveWriter.write(bytearray(json.dumps(s)+"\n","utf8"))
		asyncio.create_task(self.abletonLiveWriter.drain())

	def stopTrack(self,track):
		s = {"stop":{"track":track}}
		self.abletonLiveWriter.write(bytearray(json.dumps(s)+"\n","utf8"))
		asyncio.create_task(self.abletonLiveWriter.drain())

	def mixerSet(self,track,dev,value):
		s = {"mix":{"track":track,"device":dev,"value":value}}
		self.abletonLiveWriter.write(bytearray(json.dumps(s)+"\n","utf8"))
		asyncio.create_task(self.abletonLiveWriter.drain())

	def queueForPartner(self,pers):
		if not self.waitingForPartner:
			self.waitingForPartner = pers
			return

		if self.waitingForPartner == pers:
			return

		if not self.waitingForPartner.isWaitingForPartner():
			self.waitingForPartner = pers
			return

		print (f"CONNECTING {self.waitingForPartner.tel.livePrefix} with {pers.tel.livePrefix}")

		self.allocDialog(pers,self.waitingForPartner)
		pers.foundPartner(self.waitingForPartner)
		self.waitingForPartner.foundPartner(pers)
		self.waitingForPartner = None

	def allocDialog(self,persA,persB):
		random.shuffle(self.dialogs)
		d = self.dialogs.pop()
		persA.setDialog(d,"A")
		persB.setDialog(d,"B")

	def freeDialog(self,dialog):
		if dialog and not dialog in self.dialogs:
			self.dialogs.append(dialog)

	def __del__(self):
		self.stopTrack("all")



async def main():
	# serialNrToLiveTrack = {"5147180":"2","5147430":"8","5131000":"1","5147500":"7",
	# 					   "5130980":"9","5147350":"3","5150860":"6","5150810":"4",
	# 					   "5149780":"5","5131520":"11","5150380":"12","5130820":"10"}
	
	serialNrToLiveTrack = {"5149780":"5"}

	dialogsAndSolutions = [("1",b"3142"),("2",b"9236"),("3",b"6894"),
						   ("4",b"8241"),("5",b"6924"),("6",b"4266")]

	#dialogsAndSolutions = [("1",b"3142", "A"),("1",b"3142", "B"),("1",b"3142", "C"),
	#					   ("1",b"3142", "D"),("1",b"3142", "E"),("1",b"3142", "F")]

	for c in serial.tools.list_ports.grep("16c0:048a"):
		print(c.serial_number)
	raetsel = await Telefonraetsel(serialNrToLiveTrack, dialogsAndSolutions, "http://192.168.178.51:8080/telefone")
	while True:
		await asyncio.sleep(20)

asyncio.run(main())

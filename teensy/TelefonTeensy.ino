#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>
#include <Keypad.h>
#include <Bounce.h>

AudioOutputUSB           usb2;
AudioInputUSB            usb1;           //xy=200,69  (must set Tools > USB Type to Audio)
AudioOutputI2S           i2s1;           //xy=365,94
AudioInputI2S            i2s2;
AudioConnection          patchChord0(i2s2, usb2);
AudioConnection          patchCord1(usb1, 0, i2s1, 0);
AudioConnection          patchCord2(usb1, 1, i2s1, 1);


AudioControlSGTL5000     sgtl5000_1;     //xy=302,184

/* 15 Tasten keyboard */

/*
const byte ROWS = 4; //four rows
const byte COLS = 4; //three columns
char keys[ROWS][COLS] = {
  {'1', '2', '3', 'A'},
  {'4', '5', '6', 'B'},
  {'7', '8', '9', 'C'},
  {'*', '0', '#', 'D'}
};
byte rowPins[ROWS] = {1, 2, 8, 7}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {3, 4, 5, 6}; //connect to the column pinouts of the keypad

Keypad kpd = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS ); */


/* 12 Tasten keyboard */

const byte ROWS = 5; //four rows
const byte COLS = 5; //three columns
char keys[ROWS][COLS] = {
  {'1', '?', '2', '?', '3'},
  {'?', '4', '5', '?', '6'},
  {'?', '7', '?', '8', '9'},
  {'?', '?', '?', '0', 'A'},
  {'?', '*', '?', '?', '?'}
};
byte rowPins[ROWS] = {10, 2, 5, 7, 4}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {12, 3, 1, 6, 8}; //connect to the column pinouts of the keypad

Keypad kpd = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );

String msg;

boolean hookswitchState = false;
uint16_t i;


void setup() {
  AudioMemory(24);
  sgtl5000_1.enable();
  sgtl5000_1.volume(0.6);
  sgtl5000_1.inputSelect(AUDIO_INPUT_MIC);
  sgtl5000_1.micGain(60);
  sgtl5000_1.lineOutLevel(13);

  pinMode(15, OUTPUT);
  pinMode(16, OUTPUT);
  digitalWrite(15, LOW);
  digitalWrite(16, LOW);

  pinMode(17, INPUT_PULLUP);
  pinMode(14, INPUT_PULLUP);
}

void loop() {
  if (i++ > 20) {
    i=0;
    // read the PC's volume setting
    float vol = usb1.volume();
    sgtl5000_1.volume(vol);
  }
  // Fills kpd.key[ ] array with up-to 10 active keys.
  // Returns true if there are ANY active keys.
  if (kpd.getKeys())
  {
    for (int i = 0; i < LIST_MAX; i++) // Scan the whole key list.
    {
      if ( kpd.key[i].stateChanged )   // Only find keys that have changed state.
      {
        switch (kpd.key[i].kstate) {  // Report active key state : IDLE, PRESSED, HOLD, or RELEASED
          case PRESSED:
            msg = " PRESSED.";
            break;
          case HOLD:
            msg = " HOLD.";
            break;
          case RELEASED:
            msg = " RELEASED.";
            break;
          case IDLE:
            msg = " IDLE.";
            continue;
        }
        Serial.print("Key ");
        Serial.print(kpd.key[i].kchar);
        Serial.println(msg);
      }
    }
  }

  if (hookswitchState != digitalRead(14)) {
    if (hookswitchState)
      Serial.println("Key H PRESSED.");
    else
      Serial.println("Key H RELEASED.");
    hookswitchState = digitalRead(14);
  }

  delay(5);
}

# Telefone

## Install

Python version from 3.5 until 3.9. Versions above and below will not work.

pyserial-asyncio version 0.4. Other versions will not work.

If you already installed latest payserial-asyncio run following command

`pip install --force-reinstall -v "pyserial-asyncio==0.4"`

## Setup

> Default Settings are:
> **phone server ip:** 192.168.0.9
> **ableton local port:** 9000
> **ableton remote port:** 9010
> ** 

Adjust IP Addresses to match your network specifications in `server/telefon_static_partners.py`

Line 369
```
raetsel = await Telefonraetsel(serialNrToLiveTrack, dialogsAndSolutions, connectedPhones, "http://192.168.0.2:8080/telefone")
```

Attach all 12 Phones to the machine that will be running server and liveset.

Create an [Aggregate Soundcard Device](https://support.apple.com/en-us/102171) that includes all Teensy Soundcards.

open `konferenz 0512D.als` in Ableton Live and select the Aggregate Device.

## Run

Make sure to start Plaiframe or adaptor:ex beforehand.

open `konferenz 0512F.als` in Ableton Live.

Start the Phone Conference Python Server with fixed phone partners.

```
python3 ./server/telefon_static_partners.py
```

## Usage

In plaiframe session show view click the **START_TASK** cue to initialize the task.

![Start Task Cue](./assets/start-task.png)

The phones should now, one by one, start ringing through the speaker (**Ring**).

If you pick up an earphone the ringing for that phone should stop and a track will be audible in the earphone telling you to "Please Wait" (Warten2).

Once your *partner player* picked up the phone on his/her end a next track will be played for both of the players in the earphones saying "Please provide Code for Secure Connection" (**Code Eingeben**).

Speaker and Mic of phones are connected in pairs. The mapping is defined in `telefon_static_partners.py` *main* function.


In order to succeed each player needs to enter the number written on the respective partners phone which matches the phone numbers in the above function.

Once both players entered the correct "Security Number" they will hear another track on the earphone. Each player will hear a separate track **A** (**DIALOG_X_A**) or **B**  (**DIALOG_X_B**) that is part of a full dialog. Each player will hear only one of two characters speaking. If each player repeats the audio, they will be able to complete each others text and be able to find out what number to type in next.

### Example Dialog (**1**)

DIALOG 1 – Solution: **3142**

**A**: Track *1 Telefon OUT* Clip *1 DIALOG_1_A*

**B** Track *2 Telefon OUT* Clip *2 DIALOG_1_B*

|Voice|German|English|
|-----|------|-------|
| A| Ich habe gehört, dass sein Mutter zwei Krankheiten hat!| I have heard that his mother has two diseases!
| B | Nein, es ist sogar noch eine mehr! | No, there's even one more!
| A | Und sein Vater soll zwei Söhne haben! | And his father is said to have two sons!
| B | Nein, es ist einer weniger. | No, it's one less.
| A | Ist notiert. Was hast du über ihn gehört? | Is noted. What have you heard about him?
| B | Ich habe gehört, dass seine Schwester acht Tanten hat! | I heard that his sister has eight aunts!
| A | Nein, es ist nur die Hälfte. | No, it's only half of that.
| B | Und sein Onkel hat ein Pferd! | And his uncle has one horse!
| A | Nein, er hat sogar doppelt so viele! | No, he even has twice as many!
| B | Ist notiert. | Is noted.
| A | Nun haben wir vier Informationen über seine Familie! | Now we have four pieces of information about his family!
| B | Vier Informationen sind genug. Teilen wir sie mit den anderen. | Four pieces of information are enough. Let's share it with the others.
| A | Noch einmal zum Abgleich? | Once again for synchronisation?
| B | Noch einmal zum Abgleich. | Once again for synchronisation.

The audio will loop until any of the players type the above number. Once they did this, there will be another audio loop (**Info**) recommending to help the other player pairs solve their dialog.

Click **RESET** cue to stop the task.
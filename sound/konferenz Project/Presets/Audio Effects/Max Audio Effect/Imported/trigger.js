outlets = 1

var liveset = undefined

var buffer = {clips:{},tracks:{},scenes:{}}

function init() {
  liveset = new LiveAPI("live_set")
  preload()
}

function info(text) {
  post(text + "\n")
  outlet(0, "info", text)
}
function warn(text) {
  post(text + "\n")
  outlet(0, "warn", text)
}

/**
* handle incomming tcp message
*/
function route(msg) {
	var js_msg = JSON.parse(msg)
	for(var key in js_msg)
	{
		// info("incomming: " + key + ": " + js_msg[key])
		switch(key) {
			case "trigger":
			case "fire":
			case "start":
			case "play":
        if (typeof js_msg[key] === 'string') {
					start("clips", js_msg[key], "fire")
			  } else {
          for(var j in js_msg[key]) {
            if(j == "clip") {
              var type = "clips"
            } else if (j == "scene") {
              var type  = "scenes"
            } else {
              var type = j
            }
            call(type, js_msg[key][j], "fire")
          }
        }
				break
			case "stop":
        if(typeof js_msg[key] === "string"){
          call("clips", js_msg[key], "stop")
        } else {
          if(js_msg[key].hasOwnProperty("track")) {
            if(js_msg[key].track.toLowerCase() == "master" || js_msg[key].track.toLowerCase() == "all") {
              liveset.call("stop_all_clips")
            } else {
              call("tracks", js_msg[key].track, "stop_all_clips")
            }
          }
  				if(js_msg[key].hasOwnProperty("clip")) {
  					call("clips", js_msg[key].clip, "stop")
  				}
        }
        break
      case "mix":
        if(js_msg[key].hasOwnProperty("track") && js_msg[key].hasOwnProperty("device") && js_msg[key].hasOwnProperty("value")) {
          setMixer(js_msg[key].track, js_msg[key].device, js_msg[key].value)
        } else {
          warn("infos missing to set mixer device")
        }
        break
      case "set":
        set(js_msg[key])
        break
      case "call":
        for(var j in js_msg[key]) {
          call(j, js_msg[key].name, js_msg[key].action)
        }
        break
			default:
				warn('no valid command in tcp message', js_msg)
		}
	}
}

function call(type, name, action) {
  if(buffer[type].hasOwnProperty(name)) {
    for(var i = 0; i<buffer[type][name].length;i++) {
      buffer[type][name][i].call(action)
      info(action + " " + i+1 + ". " + type + " " + buffer[type][name][i].get("name"))
    }
  } else {
    warn(type + " not found: '" + name + "'. Hit refresh if you renamed or added " + type)
  }
}

function set(data) {
  for(var d in data) {
    if(data[d].hasOwnProperty("name") && data[d].hasOwnProperty("property") && data[d].hasOwnProperty("value")) {
      setValue(d, data[d].name, data[d].property, data[d].value)
    } else {
      warn("infos missing to set property")
    }
  }
}

function setValue(type, name, property, value) {
  if(buffer.hasOwnProperty(type)) {
    if(buffer[type].hasOwnProperty(name)) {
      for(var i = 0; i<buffer[type][name].length;i++) {
        buffer[type][name][i].set(property, value)
        info("set " + type + " " + name + " " + property + " to " + value)
      }
    } else {
      warn(type + " not found: " + name + ". Hit refresh if you changed or added " + type)
    }
  } else {
    warn("no such type: " + type + ". You can only set properties for 'clips', 'scenes' or 'tracks' ")
  }
}

function setMixer(track, device, value) {
  
  for(var i = 0; i<buffer["tracks"][track].length;i++) {
    var mixer = undefined
    var index = -1
    
    var track_path = buffer["tracks"][track][i].path.replace(/['"]/g,'')
    
    if(typeof device === 'string') {
      if(device.length == 1) {
        index = device.toUpperCase()
        index = device.charCodeAt(0) - 65
        device = "sends"
      }
    } else {
      index = device
      device = "sends"
    }
    
    if(index >= 0) {
      mixer = new LiveAPI(track_path + " mixer_device " + device + " " + index)
    } else {
      mixer = new LiveAPI(track_path + " mixer_device " + device)
    }
    
    if(mixer.id != 0) {
      mixer.set("value", value)
      if(index) {
        info("set " + device + " " + index + " value of track " + track + ": " + mixer.get("value"))
      } else {
        info("set " + device + " value of track " + track + ": " + mixer.get("value"))
      }
    } else if (index) {
      warn("no " + device + " in track " + track + " at index " + index)
    } else {
      warn("no " + device + " in track " + track)
    }
  }
}

function preload() {
  buffer = {clips:{},tracks:{},scenes:{}}
  info("loading M4L objects ... ")
  
  // Tracks
  for(var i = 0; i < liveset.get("tracks").length/2; i++) {
    var track = new LiveAPI("live_set tracks " + i)
    store("tracks", track)
    // Clips
    for(var c = 0; c < track.get("clip_slots").length/2; c++) {
      var clipslot = new LiveAPI("live_set tracks " + i + " clip_slots " + c)
      if(clipslot.get("clip")[1] != 0) {
        var clip = new LiveAPI("live_set tracks " + i + " clip_slots " + c + " clip")
        store("clips", clip)
      }
    }
  }
  // Scenes
  for(var i = 0; i < liveset.get("scenes").length/2; i++) {
    var scene = new LiveAPI("live_set scenes " + i)
    store("scenes", scene)
  }  
  
  info("Loaded " + Object.keys(buffer.tracks).length + " tracks, " + Object.keys(buffer.clips).length + " clips and " + Object.keys(buffer.scenes).length + " scenes.")
}

function store(type, object) {
  if(buffer[type].hasOwnProperty(object.get("name"))) {
    buffer[type][object.get("name")].push(object)
  } else {
    buffer[type][object.get("name")] = [object]
  }
}
